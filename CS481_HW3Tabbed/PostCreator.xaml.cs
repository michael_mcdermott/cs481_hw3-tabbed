﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Forms;

namespace CS481_HW3Tabbed
{
    public partial class PostCreator : ContentPage
    {
        int listIndex;
        private bool pageVisited;
        public string PostText {
            get; set;
        }

        public string FavStatus{
            get; set;
        }
        Post currentPost;
        public PostCreator()
        {
            listIndex = 0;
            pageVisited = false;
       
            // List of posts is created in the App.xaml.cs file and declared
            // a static variable so multiple forms can use it.
            // Same goes for the favorites list.

            //set the first post to the first index of the list
            currentPost = App.Posts[0];
            PostText = currentPost.postIdea;
            if (currentPost.favorited == true)
            {
                FavStatus = "favfilled.png";
            }
            else
            {
                FavStatus = "favnotfilled.png";
            }
            InitializeComponent();
            BindingContext = this;
        }

        async void ContentPage_Appearing(System.Object sender, System.EventArgs e)
        {
            await this.FadeTo(1500, 0); // fades page in

            if (!pageVisited)
            {
                await DisplayAlert("How To Use This Page", "Cycle through post ideas and tap the heart to save them to your favorites", "OK!").ConfigureAwait(false);
            }
        }
        async void ContentPage_Disappearing(System.Object sender, System.EventArgs e)
        {
            pageVisited = true;
            await this.FadeTo(0, 1500); // fades page out
        }

        /*LoadNextPost handles increasing the index of the list and updating
         * the text for the post. It also calls OnPropertyChanged, which lets the
         * binding data know that it has updated so it can change to the new text
         */
        void LoadNextPost(System.Object sender, System.EventArgs e)
        {
            listIndex = (listIndex + 1) % App.Posts.Count;
            currentPost = App.Posts[listIndex];
            PostText = currentPost.postIdea;
            if (currentPost.favorited == true)
            {
                FavStatus = "favfilled.png";
            }
            else
            {
                FavStatus = "favnotfilled.png";
            }
            OnPropertyChanged(nameof(PostText));
            OnPropertyChanged(nameof(FavStatus));

        }

        void Favorited(System.Object sender, System.EventArgs e)
        {
            // set the favorited value of the current post to the opposite of what it is
            currentPost.favorited = !currentPost.favorited;
            if (currentPost.favorited == true)
            {
                FavStatus = "favfilled.png";
                if (!App.FavoriteCollection.Contains(currentPost))
                {
                    App.FavoriteCollection.Add(currentPost);
                }
            }
            else
            {
                FavStatus = "favnotfilled.png";
                if (App.FavoriteCollection.Contains(currentPost))
                {
                    App.FavoriteCollection.Remove(currentPost);
                }
            }
             OnPropertyChanged(nameof(FavStatus));
        }
    }
}