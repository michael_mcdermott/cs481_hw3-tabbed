﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace CS481_HW3Tabbed
{
    public partial class FrontPage : ContentPage
    {
        public FrontPage()
        {
            InitializeComponent();
        }

        async void ContentPage_Appearing(System.Object sender, System.EventArgs e)
        {
            await this.FadeTo(1500, 0); // fades page in 

        }
        async void ContentPage_Disappearing(System.Object sender, System.EventArgs e)
        {
            await this.FadeTo(0, 1500); // fades page out 

        }
    }
}
