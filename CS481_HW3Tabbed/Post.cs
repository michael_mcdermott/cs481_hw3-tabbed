﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
namespace CS481_HW3Tabbed
{

    /* The Post class is for post objects. They all have a text string
     * and a favorited boolean. The favorited boolean will be used with another list of favorites
     */
    public class Post 
    {

        public string postIdea { get; set; }
        public Boolean favorited { get; set; }

        public Post()
        {
             this.postIdea = "";
             this.favorited = false;
        }

        public Post(string text)
        {
            this.postIdea = text;
            this.favorited = false;
        }

        public void favoritePost()
        {
            if(this.favorited == false)
            {
                this.favorited = true;
            }
        }

        public void unfavoritePost()
        {
            if (this.favorited == true)
            {
                this.favorited = false;
            }
        }

        public void editPost(string edit)
        {
            this.postIdea = edit;
        }
    }
}
