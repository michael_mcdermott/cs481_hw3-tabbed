﻿using System;
using System.Collections.Generic;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace CS481_HW3Tabbed
{
    public partial class PreferencesPage : ContentPage
    {
        private bool pageVisited;
        public bool NotificationsSetting {
            get => Preferences.Get(nameof(NotificationsSetting), true);
            set
            {
                Preferences.Set(nameof(NotificationsSetting), value);
                OnPropertyChanged(nameof(NotificationsSetting));
            }
         }

        public bool DarkModeSetting
        {
            get => Preferences.Get(nameof(DarkModeSetting), true);
            set
            {
                Preferences.Set(nameof(DarkModeSetting), value);
                OnPropertyChanged(nameof(DarkModeSetting));
            }
        }

        public bool RememberMeSetting
        {
            get => Preferences.Get(nameof(RememberMeSetting), true);
            set
            {
                Preferences.Set(nameof(RememberMeSetting), value);
                OnPropertyChanged(nameof(RememberMeSetting));
            }
        }

        public bool AnalyticsSetting
        {
            get => Preferences.Get(nameof(AnalyticsSetting), true);
            set
            {
                Preferences.Set(nameof(AnalyticsSetting), value);
                OnPropertyChanged(nameof(AnalyticsSetting));
            }
        }

        public PreferencesPage()
        {
            pageVisited = false;
            InitializeComponent();
            BindingContext = this;
        }

        async void ContentPage_Appearing(System.Object sender, System.EventArgs e)
        {
            if (!pageVisited)
            {
                await DisplayAlert("NOTE", "The toggles on this page currently alters settings variables in the app, but the variables do not actually change anything. If you clear the app, the preferences will be saved.", "OK!").ConfigureAwait(false);
            }
        }
        async void ContentPage_Disappearing(System.Object sender, System.EventArgs e)
        {
            pageVisited = true;
          //  await this.FadeTo(0, 1500); // fades page out 

        }


        //sets all preferences switches and saved preferences to false
        void ResetAllSettings(System.Object sender, System.EventArgs e)
        {
            Preferences.Clear();
            Preferences.Set(nameof(NotificationsSetting), false);
            OnPropertyChanged(nameof(NotificationsSetting));
            Preferences.Set(nameof(DarkModeSetting), false);
            OnPropertyChanged(nameof(DarkModeSetting));
            Preferences.Set(nameof(RememberMeSetting), false);
            OnPropertyChanged(nameof(RememberMeSetting));
            Preferences.Set(nameof(AnalyticsSetting), false);
            OnPropertyChanged(nameof(AnalyticsSetting));
        }

        void Switch_Toggled(System.Object sender, Xamarin.Forms.ToggledEventArgs e)
        {
            bool toggled = Preferences.Get(nameof(sender), true);
            toggled = !toggled;
            Preferences.Set(nameof(sender), toggled);
        }
    }
}
