﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace CS481_HW3Tabbed
{
    public partial class App : Application
    {
        //Favorites list declared here so it can be accessed in multiple forms
        readonly LinkedList<Post> favorites;
        public static ObservableCollection<Post> FavoriteCollection { get; private set; }

        //Posts list declared here so it can be accessed in multiple forms
        readonly LinkedList<Post> posts;
        public static ObservableCollection<Post> Posts { get; private set; }

        public App()
        {
            InitializeComponent();

            favorites = new LinkedList<Post>();
            FavoriteCollection = new ObservableCollection<Post>(favorites);

            posts = new LinkedList<Post>();
            Posts = new ObservableCollection<Post>(posts);

            //read in from file "PostIdeas.txt"
            /* THIS NEEDS TO BE FIXED TO READ IN A FILE OF IDEAS 
            var sr = new StreamReader(@"PostIdeas.txt");
            while (sr.Peek() >= 0)
                Posts.Add(new Post(sr.ReadLine()));
            */
            Posts.Add(new Post("Post a picture of your dog"));
            Posts.Add(new Post("Post an inspirational quote"));
            Posts.Add(new Post("Share a photo of a sentimental place"));
            Posts.Add(new Post("Engage with your audience"));
            Posts.Add(new Post("Comment on one of your fans pictures"));
            Posts.Add(new Post("Please, just don't post a picture of your brunch"));
            Posts.Add(new Post("Maybe just don't post on social media today?"));

            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
